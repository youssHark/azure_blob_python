import os, uuid, sys, subprocess,time, random
from azure.storage.blob import BlockBlobService, PublicAccess

def run_swarmTokenCheck():
    try:
        # Creer le BlockBlocbService utilise pour appeler le Blob service pour le the storage account
        block_blob_service = BlockBlobService(account_name='sanyh', account_key='pCWMT9YHn+TYPqg1cTwrZPi3yebMPnZMpk7vnPq9S5CTK8ig8DngDOCxCd+XETqtYAfbhvE3QUkcTw1SZzanHQ==')

        # Creer le container qui s'appelle 'petalmdblobs'.
        container_name ='petalmdblobs'
        block_blob_service.create_container(container_name)

        # Setter les permissions du blob: public/prive
        block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)

        # Lister les blobs du container
        # do this maybe 5 times with 1 second delay between tries to
        # reduce the likelihood that it would write a token at the same time
        end = False
        for x in range(0, 3):
            print("\nList blobs in the container " + str(x))
            generator = block_blob_service.list_blobs(container_name)
            for blob in generator:
                if blob.name == 'youss.txt':
                    print("File already exists. Ending")
                    blobObject = block_blob_service.get_blob_to_text(blob_name='youss.txt',container_name=container_name)
                    token = blobObject.content
                    #print(blobObject.content)
                    #consoleOut = subprocess.check_output("kill ")
                    end = True
                print("\t Blob name: " + blob.name)

            time.sleep(random.randint(1,3))
        if end:
            return

        # executer la commande swarm init pour le 1er process qui arrive ici
        # swarmInit = subprocess.run(["docker", "swarm init --advertise-addr 192.168.99.100 "]) //ip for example
        #  parser le resultat pour extraire le token a ecrire dans le fichier texte
        local_path=os.path.abspath(os.path.curdir)
        local_file_name ='youss.txt'
        full_path_to_file =os.path.join(local_path, local_file_name)

        # Write text to the file.
        file = open(full_path_to_file,  'w')
        file.write("Hello, World!")
        file.close()

        print("Temp file = " + full_path_to_file)
        print("\nUploading to Blob storage as blob" + local_file_name)

        # Upload the created file, use local_file_name for the blob name

        block_blob_service.create_blob_from_path(container_name, local_file_name, full_path_to_file)



        # Download the blob(s).
        # Add '_DOWNLOADED' as prefix to '.txt' so you can see both files in Documents.
        full_path_to_file2 = os.path.join(local_path, str.replace(local_file_name ,'.txt', '_DOWNLOADED.txt'))
        print("\nDownloading blob to " + full_path_to_file2)
        block_blob_service.get_blob_to_path(container_name, local_file_name, full_path_to_file2)

        sys.stdout.write("Sample finished running. When you hit <any key>, the sample will be deleted and the sample "
                         "application will exit.")
        sys.stdout.flush()
        input()

        # Clean up resources. This includes the container and the temp files
        block_blob_service.delete_container(container_name)
        os.remove(full_path_to_file)
        os.remove(full_path_to_file2)
    except Exception as e:
        print(e)


# Main method.
if __name__ == '__main__':
    run_swarmTokenCheck()
