#!/usr/bin/env python3
import os
from azure.storage.blob import BlockBlobService


def run_swarmtokencheck():
    try:
        # Creer le BlockBlocbService utilise pour appeler le Blob service pour le storage account
        block_blob_service = BlockBlobService(account_name='ansibledockerfiletoken',
                                              account_key='8F/E0HNKVJMSDEvNeHu52UR4QETTrY39RZlg5X7nvtOJFZ3B9NW9Mb53AB6UCi9QAfVgy/YErIKjltIebMYmqA==')

        # Creer le container qui s'appelle 'petalmdblobs'.
        container_name = 'petalmdblobs'
        blobexist = block_blob_service.exists(container_name, 'youss.txt')
        if blobexist:
            print("join")
            # indiquer variable exist pour register ansible => donc task "JOIN SWARM" pour download token
            return
        else:
            print("init")
            block_blob_service.create_container(container_name)
            local_path = os.path.abspath(os.path.curdir)
            local_file_name = 'youss.txt'
            full_path_to_file = os.path.join(local_path, local_file_name)
            # Write text to the file.
            file = open(full_path_to_file, 'w')
            file.write("Hello, PetalMD World!")
            file.close()

            # Upload the created file, use local_file_name for the blob name
            block_blob_service.create_blob_from_path(container_name, local_file_name, full_path_to_file)
            lease_id = block_blob_service.acquire_blob_lease(container_name, 'youss.txt')
            # without specifying  lease_duration=X it is infinite
            print(lease_id)
            # indiquer variable pour
    except Exception as e:
        print(e)


# Main method.
if __name__ == '__main__':
    run_swarmtokencheck()
