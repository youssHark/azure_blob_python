#!/usr/bin/env python3
import sys, time
from azure.storage.blob import BlockBlobService

def read_token_blob():
    try:
       block_blob_service = BlockBlobService(account_name='ansibledockerfiletoken',
                                             account_key='8F/E0HNKVJMSDEvNeHu52UR4QETTrY39RZlg5X7nvtOJFZ3B9NW9Mb53AB6UCi9QAfVgy/YErIKjltIebMYmqA==')
       container_name = 'petalmdblobs'
       lease_id = block_blob_service.acquire_blob_lease(container_name, 'youss.txt')
       # without specifying  lease_duration=X it is infinite
       print('Join process lease_id is '+lease_id)

       generator = block_blob_service.list_blobs(container_name)
       for blob in generator:
           if blob.name == 'youss.txt':
               print("\t Blob name: " + blob.name)
               blobObject = block_blob_service.get_blob_to_text(blob_name='youss.txt', container_name=container_name)
               token = blobObject.content
               print("\t token retrieved is: " + token)

       block_blob_service.release_blob_lease(container_name, 'youss.txt', lease_id=lease_id)
    except Exception as e:
        print(e)
        exit(1)


# Main method.
if __name__ == '__main__':
    read_token_blob()
